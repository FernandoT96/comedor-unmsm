<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Ticket;


class historialController extends Controller
{
    public function historial(Request $request){
        try{
            $role = $request->user()->role_id;
            if($role == 1){
                $tickets = Ticket::all();
                $tickets = $tickets->sortByDesc('created_at');
                return view('historial_user')->with('tickets',$tickets);
            }
            else
            $cabecera = 'Error';
            $mensaje = 'No cuentas con accesos suficientes para acceder aquí';
            return view('mensaje_operador', compact('cabecera','mensaje')); //Vista de no eres el usuario
        }
        catch(\Exception $e){
            return view('welcome'); //No estás logeado
        }
    }
}
