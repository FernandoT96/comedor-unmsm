<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Turno;



class TurnosController extends Controller
{
    public function showView(){
        $turnos = Turno::all();
        return view('actualizar_horarios')->with('turnos',$turnos);
    }

    public function guardarHorarios(Request $request){
        for($i = 1; $i<=3; $i++){
            $turno = Turno::find($i);
            $turno->entrada = $request->input('appt-time-entrada-' . $i);
            $turno->salida = $request->input('appt-time-salida-' . $i);

            $nombre_variable = 'cantidad'.(string)$i;
            $cantidad = $request->$nombre_variable;
            $turno->cantidad = $cantidad;

            $turno->save();
        }
        return $this->showView();
    }

}
