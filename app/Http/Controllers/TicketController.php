<?php

namespace App\Http\Controllers;



use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use App\Turno;
use App\Ticket;
use App\Menu;
use Carbon\Carbon;

class ticketController extends Controller
{

    public function solicitud(Request $request){
        try{
            $role = $request->user()->role_id;
            if($role == 1){
                $dias = array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
                $dia = $dias[date('N', time())];
                $horario = DB::table('menus')->where('dia',$dia)->first();
                //$horario = DB::table('menus')->where('dia','Lunes')->first();
                $turnos = Turno::all();
                // return view('solicitud_user')->with('horarios',$horarios);
                return view('solicitud_user', compact('horario','turnos'));
            }
            else{
                $cabecera = 'Error';
                $mensaje = 'No cuentas con accesos suficientes para acceder aquí';
                return view('mensaje_operador', compact('cabecera','mensaje'));
            }
        }
        catch(\Exception $e){
            return view('welcome'); //Vista de no estás logeado
        }
    }

    public function crearticket(Request $request){
        //try{
            try{
                if($this->validarCancelar($request)){
                    $cabecera = 'Mensaje';
                    $mensaje = 'Ya has creado un ticket el día de hoy y ha sido cancelado.';
                    return view('mensaje_alumno', compact('cabecera','mensaje'));
                }
            }
            catch(\Exception $e){

            }

            $role = $request->user()->role_id;
            if($role == 1){
                // Usuario
                $idUsuario = $request->user()->id;
                $user = User::find($idUsuario);
                if($user->active){
                    // Turno
                    $nombreTurno = $request->turno;
                    $turno = DB::table('turnos')->where('nombre',$nombreTurno)->first();
                    $idTurno = $turno->id;
                    $tickets_restantes = $turno->restante;

                    $turno_horario = (date('H', time()) >= 0 and date('H', time()) <= 24);

                    if($this->validarPosicion($request)){
                        if($turno_horario and $user->hasCurrentTicket == false){
                            if($tickets_restantes > 0){
                                // Actualiza que tiene un ticket activo
                                $user->hasCurrentTicket = true;
                                $user->save();

                                // Crea un nuevo ticket
                                $ticket = new Ticket();
                                $ticket->user() -> associate($user);
                                $ticket->turno() -> associate($idTurno);
                                $ticket->date = date('Y-m-d H:i:s', time());
                                $ticket->flag_activo = true;
                                $ticket->flag_cancelado = false;
                                $ticket->flag_strike = false;
                                $ticket->save();

                                // Actualiza raciones del turno escogido
                                $turno->restante = $turno->restante - 1;
                                $turno_base = Turno::find($turno->id);
                                $turno_base->restante = $turno->restante;
                                $turno_base->save();

                                // Crea mensaje de respuesta
                                $cabecera = 'Mensaje';
                                $mensaje = 'El ticket ha sido creado satisfactoriamente.';
                                return view('mensaje_alumno', compact('cabecera','mensaje'));
                            }
                            else{
                                $cabecera = 'Tickets acabados';
                                $mensaje = 'Se terminaron los tickets en este turno.';
                                return view('mensaje_alumno', compact('cabecera','mensaje'));
                            }
                        }
                        else{
                            $cabecera = 'Error';
                            $mensaje = 'Ya tienes un ticket creado.';
                            return view('mensaje_alumno', compact('cabecera','mensaje'));
                        }
                    }
                    else{
                        $cabecera = 'Error';
                        $mensaje = 'No estás dentro de la universidad';
                        return view('mensaje_operador', compact('cabecera','mensaje'));
                    }
                }
                else{
                    $cabecera = 'Mensaje';
                    $mensaje = 'Has sido inhabilitado para pedir tickets';
                    return view('mensaje_operador', compact('cabecera','mensaje'));
                }
            }
            else{
                $cabecera = 'Error';
                $mensaje = 'No cuentas con accesos suficientes para acceder aquí';
                return view('mensaje_operador', compact('cabecera','mensaje'));
            }
       /* }
        catch(\Exception $e){
            return view('welcome'); //Vista de no estás logeado
        }*/
    }

    private function validarCancelar(Request $request){
        $role = $request->user()->role_id;
        $idUsuario = $request->user()->id;

        //Obtiene la fecha del ultimo ticket
        $last_ticket = Ticket::where('user_id', $idUsuario)->latest()->first();
        //var_dump($last_dateTicket->date);
        //var_dump(strtotime($last_dateTicket->date));

        $last_dateTicket = strtotime($last_ticket->date);
        // strtotime()
        //Obtiene la fecha actual del momento
        //$mytime = Carbon::now();

        if($last_ticket->flag_cancelado){
            if(
                (date('d', time()) == date('d', $last_dateTicket) and
                date('m', time()) == date('m', $last_dateTicket) and
                date('y', time()) == date('y', $last_dateTicket)
                )
                ){
                    //var_dump('Debio borrar');
                    return true;
                }
                else{
                    return false;
                }
        }
        else{
            return false;
        }

    }

    private function validarPosicion(Request $request){
        $longitud = (float) $request->longitud;
        $latitud = (float) $request->latitud;
        $exactitud = (float) $request->exactitud;

        //var_dump($latitud);
        //var_dump($longitud);

        /*
        //Amezaga con Colonial
        $ALat = -12.051365;
        $ALon = -77.085154;

        // Chifa puerta 8
        $BLat = -12.051835;
        $BLon = -77.087927;

        // Huaca de San Marcos
        $DLat = -12.061714;
        $DLon = -77.089671;

        // Puerta 2
        $CLat = -12.060149;
        $CLon = -77.079060;
        */

        // Latitud y Longitud de prueba
        $latitud = -12.055964;
        $longitud = -77.085421;
        //var_dump($latitud);
        //var_dump($longitud);
        $point = $latitud . " " . $longitud;
        $polygon = array("-12.051443 -77.085114", "-12.051835 -77.087927", "-12.061714 -77.089671", "-12.059428 -77.076381", "-12.051443 -77.085114");
        //return true;

        if($this->pointInPolygon($point, $polygon)){
            return true;
        }
        else{
            return false;
        }
    }

    private function pointInPolygon($point, $polygon, $pointOnVertex = true) {
        $this->pointOnVertex = $pointOnVertex;

        // Transform string coordinates into arrays with x and y values
        $point = $this->pointStringToCoordinates($point);
        $vertices = array();
        foreach ($polygon as $vertex) {
            $vertices[] = $this->pointStringToCoordinates($vertex);
        }

        // Check if the point sits exactly on a vertex
        if ($this->pointOnVertex == true and $this->pointOnVertex($point, $vertices) == true) {
            //return "vertex";
            return true;
        }

        // Check if the point is inside the polygon or on the boundary
        $intersections = 0;
        $vertices_count = count($vertices);

        for ($i=1; $i < $vertices_count; $i++) {
            $vertex1 = $vertices[$i-1];
            $vertex2 = $vertices[$i];
            if ($vertex1['y'] == $vertex2['y'] and $vertex1['y'] == $point['y'] and $point['x'] > min($vertex1['x'], $vertex2['x']) and $point['x'] < max($vertex1['x'], $vertex2['x'])) { // Check if point is on an horizontal polygon boundary
                //return "boundary";
                return true;
            }
            if ($point['y'] > min($vertex1['y'], $vertex2['y']) and $point['y'] <= max($vertex1['y'], $vertex2['y']) and $point['x'] <= max($vertex1['x'], $vertex2['x']) and $vertex1['y'] != $vertex2['y']) {
                $xinters = ($point['y'] - $vertex1['y']) * ($vertex2['x'] - $vertex1['x']) / ($vertex2['y'] - $vertex1['y']) + $vertex1['x'];
                if ($xinters == $point['x']) { // Check if point is on the polygon boundary (other than horizontal)
                    //return "boundary";
                    return true;
                }
                if ($vertex1['x'] == $vertex2['x'] || $point['x'] <= $xinters) {
                    $intersections++;
                }
            }
        }
        // If the number of edges we passed through is odd, then it's in the polygon.
        if ($intersections % 2 != 0) {
            //return "inside";
            return true;
        } else {
            //return "outside";
            return false;
        }
    }

    private function pointStringToCoordinates($pointString) {
        $coordinates = explode(" ", $pointString);
        return array("x" => (float)$coordinates[0], "y" => (float)$coordinates[1]);
    }

    private function pointOnVertex($point, $vertices) {
        foreach($vertices as $vertex) {
            if ($point == $vertex) {
                return true;
            }
        }
    }
}



