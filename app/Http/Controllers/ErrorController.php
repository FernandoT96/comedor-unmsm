<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;

class ErrorController extends Controller
{
    public function showView(){
        $data = array(
            'codigo' => '1',
            'nombre' => 'Error'
        );
        $tickets = User::all();
        return view('error')->with('data', $data);//.blade.php
    }
}
