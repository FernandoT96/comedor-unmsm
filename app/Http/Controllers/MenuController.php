<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;




class menuController extends Controller
{
    public function show(Request $request){
        try{
            $role = $request->user()->role_id;
            if($role == 1){
                $horarios = Menu::all();
                $number_day = date('N', time())-1;

                $init_day = date('d', strtotime('-'.$number_day.' days'));
                $final_day = date('d', strtotime('+'.(6-$number_day).' days'));

                $month = now()->format('F');

                $fecha = 'Del '.$init_day.' al '.$final_day.' de junio';
                return view('menu_user', compact('fecha','horarios'));
            }
            else
            $cabecera = 'Error';
            $mensaje = 'No cuentas con accesos suficientes para acceder aquí';
            return view('mensaje_operador', compact('cabecera','mensaje')); //Vista de no eres el usuario
        }
        catch(\Exception $e){
            return view('welcome'); //No estás logeado
        }
    }

    public function crearmenu(){
        $menu = new Menu();
        $menu->dia = 'Lunes';
        $menu->entrada = 'Ensalada rusa';
        $menu->almuerzo = 'Asado de red';
        $menu->postre = 'Manzana';
        $menu->save();
    }

    public function actualizarmenu($dia){
        $menu = Menu::find($dia);
        $menu->dia = 'Lunes';
        $menu->entrada = 'Ensalada rusa';
        $menu->almuerzo = 'Asado de red';
        $menu->postre = 'Platano';
        $menu->save();
    }

    public function detallemenu($dia){
        $menu = Menu::find($dia);
        return 'El dia ' . $menu->dia . ' se servira ' . $menu->entrada . ' como entrada';
    }

    public function listarmenu(){
        $menu = Menu::all();
        $dias = $menu->dia;
        return $dias;
    }

    public function getmax(){
        $menu = Menu::all()->max('dia');
        return $menu->dia;
    }

    public function showEditar(Request $request){
        try{
            $role = $request->user()->role_id;
            if($role == 2){
                $horarios = Menu::all();
                $number_day = date('N', time())-1;

                $init_day = date('d', strtotime('-'.$number_day.' days'));
                $final_day = date('d', strtotime('+'.(6-$number_day).' days'));

                $month = now()->format('F');

                $fecha = 'Del '.$init_day.' al '.$final_day.' de junio';
                return view('menu_operario', compact('fecha','horarios'));
            }
            else
            $cabecera = 'Error';
            $mensaje = 'No cuentas con accesos suficientes para acceder aquí';
            return view('mensaje_alumno', compact('cabecera','mensaje')); //Vista de no eres el usuario
        }
        catch(\Exception $e){
            return view('welcome'); //No estás logeado
        }
    }

    public function guardarMenu(Request $request){
        $menus = Menu::all();
        foreach($menus as $menu){
            $dia = $menu->dia;

            $menu->entrada = $request->input('entrada' . $dia);
            $menu->almuerzo = $request->input('almuerzo' . $dia);
            $menu->postre = $request->input('postre' . $dia);
            $menu->save();
        }
        return $this->showEditar($request);
    }

}
