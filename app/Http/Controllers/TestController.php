<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;




class testController extends Controller
{
    public function test(){
        return view('test');
    }

    public function getpos(Request $request){
        $tipo = $request->tipo;
        if($tipo == "almuerzo"){
            return "saco su ticket de almuerzo";
        }
        elseif($tipo == "cena"){
            return "saco su ticket de cena";
        }
    }

    public function test_post(Request $request){
        return view('test_post');
    }
}
