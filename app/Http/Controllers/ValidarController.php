<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Ticket;
use App\User;




class validarController extends Controller
{
    public function validar(Request $request){
        try{
            $role = $request->user()->role_id;
            if($role == 2)
                return view('validar_ticket');
            else{
                $cabecera = 'Error';
                $mensaje = 'No cuentas con accesos suficientes para acceder aquí';
                return view('mensaje_alumno', compact('cabecera','mensaje'));
            }
        }
        catch(\Exception $e){
            return view('welcome'); //Vista de no estás logeado
        }
    }

    public function check(Request $request){
        try{
            $role = $request->user()->role_id;
            if($role == 2)
                {
                    //Desde el Request
                    $idTicket = $request->input('codigoTicket');
                    $idUser = $request->user()->id;

                    //Cargar todos los ids de tickets
                    try{
                        $ticket = Ticket::find($idTicket);
                        //Revisamos si el ticket existe o no
                        if($ticket->flag_activo == false){
                            $cabecera = 'Error';
                            $mensaje = 'El ticket ya fue utilizado.';
                            return view('mensaje_operador', compact('cabecera','mensaje'));
                        }
                        else{
                            $user = User::find($idUser);
                            $user->hasCurrentTicket = false;
                            $user->save();

                            $ticket->flag_activo = false;
                            $ticket->save();

                            $cabecera = 'Mensaje';
                            $mensaje = 'El alumno puede pasar.';
                            return view('mensaje_operador', compact('cabecera','mensaje'));
                        }
                    }
                    catch (\Exception $e){
                        $cabecera = 'Error';
                        $mensaje = 'No existe ningun ticket con ese número';
                        return view('mensaje_operador', compact('cabecera','mensaje'));
                    }
                }
            else{
                $cabecera = 'Error';
                $mensaje = 'No cuentas con accesos suficientes para acceder aquí';
                return view('mensaje_alumno', compact('cabecera','mensaje'));
            }
        }
        catch(\Exception $e){
            return view('welcome'); //Vista de no estás logeado
        }
    }


    public function check_url(Request $request, $idTicket = null){
        try{
            $role = $request->user()->role_id;
            if($role == 2)
                {
                    //Desde el Request
                    //$idTicket = $request->input('codigoTicket');
                    $idUser = $request->user()->id;

                    //Cargar todos los ids de tickets
                    try{
                        $ticket = Ticket::find($idTicket);
                        //Revisamos si el ticket existe o no
                        if($ticket->flag_activo == false){
                            if($ticket->flag_cancelado == false){
                                $cabecera = 'Error';
                                $mensaje = 'El ticket ya fue utilizado.';
                                return view('mensaje_operador', compact('cabecera','mensaje'));
                            }
                            else{
                                $cabecera = 'Error';
                                $mensaje = 'El ticket ya ha sido cancelado.';
                                return view('mensaje_operador', compact('cabecera','mensaje'));
                            }
                        }
                        else{
                            $user = User::find($idUser);
                            $user->hasCurrentTicket = false;
                            $user->save();

                            $ticket->flag_activo = false;
                            $ticket->save();

                            $codigo_nombre = $user->id.'-'.$user->nombre;
                            $cabecera = 'Mensaje';
                            $mensaje = 'El alumno '.$codigo_nombre.' puede pasar.';
                            return view('mensaje_operador', compact('cabecera','mensaje'));
                        }
                    }
                    catch (\Exception $e){
                        $cabecera = 'Error';
                        $mensaje = 'No existe ningun ticket con ese número';
                        return view('mensaje_operador', compact('cabecera','mensaje'));
                    }
                }
            else{
                $cabecera = 'Error';
                $mensaje = 'No cuentas con accesos suficientes para acceder aquí';
                return view('mensaje_alumno', compact('cabecera','mensaje'));
            }
        }
        catch(\Exception $e){
            return view('welcome'); //Vista de no estás logeado
        }
    }
}
