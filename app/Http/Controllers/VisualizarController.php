<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\Turno;
use App\User;




class visualizarController extends Controller
{
    public function visualizar(Request $request){
        try{
            $userid = $request->user()->id;
            $role = $request->user()->role_id;
            if($role == 1)
            {
                try{
                    $ticket_id = Ticket::where('user_id', $userid)->latest()->first()->id;
                    $turno_id = Ticket::where('user_id', $userid)->latest()->first()->turno_id;
                    $flag_activo = Ticket::where('user_id', $userid)->latest()->first()->flag_activo;
                    $hasTicket = User::find($userid)->hasCurrentTicket;

                    $turno = Turno::where('id', $turno_id)->first();
                    $turno_nombre = $turno->nombre;
                    $hora_entrada = date('H:i', strtotime($turno->entrada));
                    $hora_salida = date('H:i a', strtotime($turno->salida));

                    $horario = $hora_entrada." - ".$hora_salida;
                }
                catch(\Exception $e){
                    $ticket_id = '';
                    $turno_id = '';
                    $turno_nombre = '';
                    $horario = '';
                    $flag_activo = false;
                    $hasTicket = false;
                }
                return view('visualizar_user', compact('ticket_id','turno_id','turno_nombre','hasTicket','horario','flag_activo'));
            }
            else{
                $cabecera = 'Error';
                $mensaje = 'No cuentas con accesos suficientes para acceder aquí';
                return view('mensaje_operador', compact('cabecera','mensaje'));
            }
        }
        catch(\Exception $e){
            return view('welcome'); //Vista de no estás logeado
        }
    }
}

