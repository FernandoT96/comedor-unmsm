<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $role = $request->user()->role_id;
            if($role == 1){
                $horarios = Menu::all();

                $number_day = date('N', time())-1;

                $init_day = date('d', strtotime('-'.$number_day.' days'));
                $final_day = date('d', strtotime('+'.(6-$number_day).' days'));

                $month = now()->format('F');

                $fecha = 'Del '.$init_day.' al '.$final_day.' de junio';
                return view('menu_user', compact('fecha','horarios'));
            }
            else{
                return view('validar_ticket');
            }
        }
        catch(\Exception $e){
            return view('welcome'); //No estás logeado
        }
    }
}
