<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Turno;



class RacionesController extends Controller
{
    public function showView(){
        $turnos = Turno::all();
        return view('actualizar_raciones')->with('turnos',$turnos);
    }

    public function actualizarTurno(Request $request){
        $idTurno = $request->turno;
        $cantidad = $request->cantidad;

        $turno = Turno::find($idTurno);
        $turno->cantidad = $cantidad;
        $turno->save();

        return $this->showView();
    }

    public function actualizarTurnos(Request $request){
        $turnos = Turno::all();
        foreach($turnos as $turno){
            $id = $turno->id;
            $variable = 'cantidad'.(string)$id;
            $cantidad = $request->$variable;

            $turno->cantidad = $cantidad;
            $turno->save();
        }
        return $this->showView();
    }
}
