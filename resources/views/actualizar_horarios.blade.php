
@extends('layouts.masterAlumno')

@section('content')
<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">


            <ul class="list-unstyled components">
                <p></p>
                <div></div>
                    <li>
                    <a href="/validar-ticket">Validar Ticket</a>
                </li>
                <div></div>
                <li>
                    <a href="/actualizar-menu">Actualizar Menú</a>
                </li>
                <div></div>
                <li class="active">
                    <a href="/actualizar-horarios">Actualizar Horarios</a>
                </li>
                <div></div>
                <li>
                    <a href="/suspender-alumno">Suspender Alumno</a>
                </li>
            </ul>


        </nav>
        <!-- Page Content  -->


        <div id="content">


            <h1 class="display-5"><center> <b>Actualizar Horarios</b></center></h1>


            <div class="line"></div>
            &nbsp

    <center>
        <div class="container">
            <form method="POST" action="/guardar-horarios">
                @csrf
                <div class="table-responsive">
                <table class="table table-striped">
                    <tr class="table-light">
                        <th scope="row">ID</th>
                        <th scope="row">Nombre</th>
                        <th scope="row">Hora entrada</th>
                        <th scope="row">Hora salida</th>
                        <th scope="row">Cantidad</th>
                        <th scope="row">Restante</th>
                    </tr>

                    @foreach($turnos as $turno)
                        <tr>
                            <td>{{$turno->id}}</td>
                            <td>{{$turno->nombre}}</td>

                            <td><input id="appt-time" type="time" class="form-control" name="appt-time-entrada-{{$turno->id}}" value={{$turno->entrada}}></td>

                            <td><input id="appt-time" type="time" class="form-control" name="appt-time-salida-{{$turno->id}}" value={{$turno->salida}}></td>
                            <td><input name="cantidad{{$turno->id}}" type="number" class="form-control" min="1" max="10000" value={{$turno->cantidad}}></td>
                            <td>{{$turno->restante}}</td>
                        </tr>
                    @endforeach
                </table>
                </div>

                <button type="submit" class="btn btn-danger">Editar</button>
            </form>
    </center>

    <div class="line"></div>
    </div>
    </div>



    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>
@stop
