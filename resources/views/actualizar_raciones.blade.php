
@extends('layouts.masterAlumno')

@section('content')
<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">


            <ul class="list-unstyled components">
                <p></p>
                <div></div>
                    <li>
                    <a href="/validar-ticket">Validar Ticket</a>
                </li>
                <div></div>
                <li class="active">
                    <a href="/actualizar-menu">Actualizar Menú</a>
                </li>
                <div></div>
                <li>
                    <a href="/actualizar-horarios">Actualizar Horarios</a>
                </li>
                <div></div>
                <li>
                    <a href="/suspender-alumno">Suspender Alumno</a>
                </li>
            </ul>

        </nav>

        <!-- Page Content  -->

        <div id="content">


            <h1 class="display-5"><center> <b>Actualizar número de raciones</b></center></h1>


            <div class="line"></div>
            &nbsp

    <center>
        <div class="container">
            <form method="POST" action="/actualizar-turnos">
            @foreach($turnos as $turno)
                <div class="row">
                    <div class="col">
                        @csrf
                        <input name="turno" type="hidden" value="{{$turno->id}}">
                        <p>{{$turno->nombre}}: <input name="cantidad{{$turno->id}}" type="number" min="1" max="10000" value={{$turno->cantidad}}> raciones.</p>
                    </div>
                </div>
                &nbsp
            @endforeach
            <p></p>
            <button type="submit" class="btn btn-danger">Actualizar</button>
            </form>
    </center>

        <div class="line"></div>
        </div>
    </div>



    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>
@stop
