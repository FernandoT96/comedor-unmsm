@extends('layouts.masterAlumno')

@section('content')
<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">

            <ul class="list-unstyled components">
                <li>
                    <a href="/menu">Menú Semanal</a>
                </li>
                <div></div>
                <li class = "active">
                    <a href="/solicitar-ticket">Solicitar Ticket</a>
                </li>
                <div></div>
                <li>
                    <a href="/visualizar-ticket">Visualizar Ticket</a>
                </li>
                <div></div>
                <li>
                    <a href="/historial-ticket">Historial de Tickets</a>
                </li>
            </ul>


        </nav>

        <!-- Page Content  -->
        <div id="content">

            <h1 class="display-5"><center><b>Solicitar Ticket</b></center></h1>

            <div class="line"></div>
                <div class="container">
                    <center><p>Recuerda que debes estar dentro de la universidad para poder solicitar un ticket.</p></center>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead class="thead-dark">
                                <tr>
                                    <th scope="col"></th>
                                        <th scope="col">{{$horario->dia}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="table-light">
                                    <th scope="row">Entrada</th>
                                            <td>{{$horario->entrada}}</td>
                                </tr>
                                <tr class="table-light">
                                    <th scope="row">Segundo</th>
                                            <td>{{$horario->almuerzo}}</td>
                                </tr>
                                <tr class="table-light">
                                    <th scope="row">Postre</th>
                                            <td>{{$horario->postre}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <script src = "/js/locationAPI.js" type="text/javascript"></script>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="elegirTurno">Elija su turno: </label>
                                <form method="POST" action="/crear">
                                    @csrf
                                    <input class="form-control"  id="latitud" name="latitud" type="hidden" required>
                                    <input class="form-control"  id="longitud" name="longitud" type="hidden" required>
                                    <input class="form-control" id="exactitud" name="exactitud"  type="hidden" required>
                                    <select class="form-control" id="exampleFormControlSelect1" name="turno">
                                    @foreach($turnos as $turno)
                                    <option label='{{$turno->nombre." (".date("H:i", strtotime($turno->entrada))." - ".date("H:i a", strtotime($turno->salida)).")"}}'>{{$turno->nombre}}</option>
                                    @endforeach
                                    </select>
                                    <br>
                                    <center><div><button type="submit" class="btn btn-danger">Solicitar</button></div></center>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="line"></div>
        </div>
    </div>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>
@stop
