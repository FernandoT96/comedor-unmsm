@extends('layouts.masterAlumno')

@section('content')

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">


            <ul class="list-unstyled components">
                <p></p>
                <div></div>
                    <li>
                    <a href="/validar-ticket">Validar Ticket</a>
                </li>
                <div></div>
                <li>
                    <a href="/actualizar-menu">Actualizar Menú</a>
                </li>
                <div></div>
                <li>
                    <a href="/actualizar-horarios">Actualizar Horarios</a>
                </li>
                <div></div>
                <li>
                    <a href="/cargar-strikes">Cargar Strikes</a>
                </li>
            </ul>


        </nav>

        <!-- Page Content  -->
        <div id="content">

            <h1 class="display-5"><center><b>Location</b></center></h1>

            <div class="line"></div>
            <div class="container">
                <script src = "/js/locationAPI.js" type="text/javascript">


                </script>
                <section id="ubicacion">
                    <div class="form-group">
                        <label class="control-label" for="latitud">Latitud</label>
                        <input class="form-control"  id="latitud" name="latitud" required>

                    </div>
                    <div class="form-group">
                        <label class="control-label" for="longitud">Longitud</label>
                        <input class="form-control"  id="longitud" name="longitud" required>

                    </div>
                    <div class="form-group">
                        <label class="control-label" for="exactitud">Exactitud </label>
                        <input class="form-control" id="exactitud" name="exactitud" id="nombre" placeholder="" required>

                    </div>
                </section>
            </div>
            <div class="line"></div>
        </div>
    </div>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>
@stop
