<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Comedor UNMSM</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        
        <!-- Styles -->
        <style>

            @import "https://fonts.googleapis.com/css?family=Oswald:300,400,500,600,700";
            html, body {
                background-color: #fff;
                color: #636b6f;
                background-image: url("comedor4.png");
                 background-size:cover;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {

                font-family: 'Oswald', sans-serif;
                font-size: 50px;
                background-color: #fff;
                border: thin;
                border-radius: 10px;
                -webkit-background-clip: text;
                -moz-background-clip: text;
                background-clip: text;
                color: transparent;
                text-shadow: rgba(255,255,255,0.5) 0px 3px 3px;
            }

            .descripcion {
                font-size: 25px;
                color: #FFFFFF;
            }

            .links > a {
                color: #000000;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}" style="color: #FFFFFF">Home</a>
                    @else
                        <a href="{{ route('login') }}" style="color: #FFFFFF">Ingresar</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" style="color: #FFFFFF">Registrarse</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <b>Sistema de tickets del comedor</b>
                </div>
                <div class="descripcion">
                    <p>Bienvenido al sistema de tickets del comedor. ¿Qué se te apetece hoy?</p>
                </div>
            </div>
        </div>
    </body>
</html>
