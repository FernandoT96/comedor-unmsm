@extends('layouts.masterAlumno')

@section('content')
<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">

            <ul class="list-unstyled components">
                <p></p>
                <div></div>
                    <li>
                    <a href="/validar-ticket">Validar Ticket</a>
                </li>
                <div></div>
                <li class="active">
                    <a href="/actualizar-menu">Actualizar Menú</a>
                </li>
                <div></div>
                <li>
                    <a href="/actualizar-horarios">Actualizar Horarios</a>
                </li>
                <div></div>
                <li>
		    <a href="/suspender-alumno">Suspender Alumno</a>
		</li>
            </ul>


        </nav>

        <!-- Page Content  -->
        <div id="content">


            <h1 class="display-5"><center><b>Actualizar Menú</b></center></h1>

            <div class="line"></div>
            <center><p>Recuerda llenar los campos para todos los días.</p></center>
            <center><p>{{$fecha}}</p></center>
            <div class="table-responsive">
                <form method="POST" action="/guardar-menu">
                    @csrf
                    <table class="table table-striped">
                        <thead class="table-light">
                            <tr class="table-light">
                                <th scope="col"></th>
                                @foreach($horarios as $horario)
                                    <th scope="col">{{$horario->dia}}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="table-light">
                                <th scope="row">Entrada</th>
                                    @foreach($horarios as $horario)
                                        <td><input name="entrada{{$horario->dia}}" type="text" class="form-control" value="{{$horario->entrada}}" autocomplete="off"></td>
                                    @endforeach
                            </tr>
                            <tr class="table-light">
                                <th scope="row">Segundo</th>
                                    @foreach($horarios as $horario)
                                        <td><input name="almuerzo{{$horario->dia}}" type="text" class="form-control" value="{{$horario->almuerzo}}" autocomplete="off"></td>
                                    @endforeach
                            </tr>
                            <tr class="table-light">
                                <th scope="row">Postre</th>
                                    @foreach($horarios as $horario)
                                        <td><input name="postre{{$horario->dia}}" type="text" class="form-control" value="{{$horario->postre}}" autocomplete="off"></td>
                                    @endforeach
                            </tr>
                        </tbody>
                    </table>

                    <center><button type="submit" class="btn btn-danger">Guardar</button></center>
                </form>
            </div>

</div>

<!-- jQuery CDN - Slim version (=without AJAX) -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<!-- Popper.JS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
<!-- Bootstrap JS -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#sidebarCollapse').on('click', function () {
            $('#sidebar').toggleClass('active');
        });
    });
</script>
</body>
@stop
