@extends('layouts.masterAlumno')

@section('content')

<body>
    <div class="wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">

            <ul class="list-unstyled components">
                <li>
                    <a href="/menu">Menú Semanal</a>
                </li>
                <div></div>
                <li>
                    <a href="/solicitar-ticket">Solicitar Ticket</a>
                </li>
                <div></div>
                <li class="active">
                    <a href="/visualizar-ticket">Visualizar Ticket</a>
                </li>
                <div></div>
                <li>
                    <a href="/historial-ticket">Historial de Tickets</a>
                </li>

            </ul>


        </nav>

        <!-- Page Content  -->
        <div id="content">

            <h1 class="display-5"><center><b>Visualizar Ticket</b></center></h1>

            <div class="line"></div>
                <div>
                @if($hasTicket == true)
                <div class="container">
                <div class="row">
                    <div class="col-sm">
                    <center>
                    {!!
                        QrCode::size(200)->generate('/check-url/'.$ticket_id);
                    !!}

                    </center>
                    </div>
                    <div class="col-sm">
                        <div class="table-responsive">
                            <table class="table table-striped">
                            <tbody>
                            <tr class="table-light">
                            <th scope="row">ID ticket</th>
                                    <td>{{$ticket_id}}</td>
                            </tr>
                            <tr class="table-light">
                            <th scope="row">Turno</th>
                                    <td>{{$turno_nombre}}</td>
                            </tr>
                            <tr class="table-light">
                            <th scope="row">Horario</th>
                                    <td>{{$horario}}</td>
                            </tr>
                            <tr class="table-light">
                            <th scope="row">Estado</th>
                                    @if($flag_activo == 1)
                                        <td>No canjeado</td>
                                    @else
                                        <td>Canjeado</td>
                                    @endif
                            </tr>
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                </div>
                    <div class="row">
                        <form method="POST" action="/cancelar">
                            @csrf
                            <input type="hidden" name="id" value="{{$ticket_id}}">
                            <button type="submit" class="btn btn-danger  btn-block btn-responsive centrados">Anular Ticket</button>
                        </form>
                    </div>
                @else
                    <div class="respText">Usted no cuenta con ningún ticket</div>
                @endif
                </div>
            <div class="line"></div>

        </div>
    </div>

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>
@stop
