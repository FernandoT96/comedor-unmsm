
function setUpBotonGetPos(){
     obtenerPos();
 }

 function obtenerPos(){
     let geoconfig={
         enableHighAccuracy: true,
         timeout: 10000,
         maximumAge: 60000
         };

     navigator.geolocation.getCurrentPosition(mostrarUbicacion,manejarErrores,geoconfig);
 }

 function mostrarUbicacion(posicion){
     document.getElementById('latitud').value = posicion.coords.latitude;
     document.getElementById('longitud').value = posicion.coords.longitude;
     document.getElementById('exactitud').value = posicion.coords.accuracy;
 }

 function manejarErrores(error){
     alert('Error: '+error.code+' '+error.message);
 } 

 window.addEventListener('load', setUpBotonGetPos, false);